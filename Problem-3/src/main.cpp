// ================================Problem 3 main===============================
// Source code to attempt to solve Project Euler problem 3
//  see https://projecteuler.net/problem=3
//  Problem description: largest prime factor of 600851475143
//  NOTE: fastest algorithm not being implemented, easy to understand algorithm
//   implemented instead.  Finds primes less than input, then calculates which
//   are divisors
// -----------------------------------------------------------------------------
// Copyright 2018 David S. H. Turner
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.
// =============================================================================

#include <iostream>
#include <thread>
#include <functional>
#include <mutex>
#include <chrono>

// ===============================Start Functions===============================
// --------------------------------Start cmdPause-------------------------------
/// Pauses command line to wait for input
void cmdPause(){
  std::cout << "Press ENTER to continue" << std::endl;
  std::string temp;
  std::getline(std::cin, temp);
}
// ---------------------------------End cmdPause--------------------------------

// --------------------------------start isPrime--------------------------------
/// \param[in]  n       Number to check
/// \param[in]  d       Divisor
/// \param[in]  result  false if divisor, unchanged if not divisor
template <typename T>
void falseIfDivisor(T n, T d, bool& result) {
  static_assert(std::is_unsigned<T>());
  if((0 == (n % d)) && (d != 1)) result = false;
}
// ---------------------------------end isPrime---------------------------------
// ================================End Functions================================

#define NUMBER_THREADS std::thread::hardware_concurrency()
// ===========================Start PrimeCalcThreaded===========================
template <typename T>
class PrimeCalcThreaded {
// -----------------------------------asserts-----------------------------------
  static_assert(std::is_unsigned<T>());
// ----------------------------------functions----------------------------------
public:
  // --------------------------------constructor--------------------------------
  PrimeCalcThreaded() {
    // first prime is 1
    _primeResults.push_back(1);
    // start pooled threads
    for(int i = 0; i < NUMBER_THREADS; i++) {
      std::thread t([this]{poolFunctionCall();});
      _threadPool.push_back(std::move(t));
    }
  }
  // ---------------------------------destructor--------------------------------
  ~PrimeCalcThreaded() {
    // wait for thread pool to finish
    _shutdownPool = true;
    for(auto it = _threadPool.begin(); it != _threadPool.end(); it++)
    {
      (*it).join();
    }
  }
  // -------------------------------calculateNext-------------------------------
  // uses a simple and dumb method of calculating primes
  ///  \return        next prime
  T calculateNext(bool printNumbers) {
    T i = _primeResults.back()+1;
    while(!isPrime(i) && (i < std::numeric_limits<T>::max())) i++;
    // assume maximum numeric limit is not prime
    if(i != std::numeric_limits<T>::max()) {
      _primeResults.push_back(i);
      if(printNumbers) std::cout << "," << i;
    }
    return _primeResults.back();
  }
  // --------------------------calculateUntilSurpassed--------------------------
  // calculates primes recursively until max is reached
  /// \param[in]  max   maximum number checked for prime-ness
  /// \return           last calculated prime (will be under max)
  T calculateUntilSurpassed(T max, bool printNumbers) {
    if(printNumbers) std::cout << _primeResults.back();
    for(T i = _primeResults.back()+1; i <= max; i++) {
      if(isPrime(i)) {
        _primeResults.push_back(i);
        if(printNumbers) std::cout << "," << i;
      }
    }
    std::cout << std::endl;
    return *(_primeResults.end());
  }
  // ------------------------------getCurrentResult-----------------------------
  /// \return         the last calculated prime number
  T getCurrentResult() {
    return *(_primeResults.end());
  }
  // ------------------------------getCurrentResult-----------------------------
  /// \return         vector containing all calculated prime numbers
  std::vector<T> getAllResults() {
    return _primeResults;
  }
  // ------------------------------getCurrentResult-----------------------------
  /// \param[in]   in   number which will have its prime divisors found
  /// \return           vector containing the found prime divisors (from those
  ///                     that have been previously calculated and stored)
  std::vector<T> commonDivisorCheck(T in) {
    std::vector<T> result;
    for(auto it = _primeResults.begin(); it != _primeResults.end(); it++) {
      if(0 == (in % *it)) {
        result.push_back(*it);
      }
    }
    return result;
  }

private:
  // --------------------------------isPoolEmpty--------------------------------
  //  checks if the function pool is empty in a safe way
  bool isPoolEmpty() {
    std::unique_lock<std::mutex>
      poolMutex(_threadFunctionPoolMutex, std::defer_lock);
    poolMutex.lock();
    return _threadFunctionPool.empty();
  }
  // ----------------------------------isPrime----------------------------------
  // checks a number against stored primes.  Not a guarantee of primeness unless
  // primes are being iterated through
  /// \return next prime
  bool isPrime(T n) {
      bool result = true;
      for(auto it = _primeResults.begin(); it != _primeResults.end(); it++) {
        std::unique_lock<std::mutex>
          poolMutex(_threadFunctionPoolMutex, std::defer_lock);
        poolMutex.lock();

        auto tmp = *it;
        // function will only change result to false if it is a divisor
        _threadFunctionPool.push_back([n,tmp,&result]{falseIfDivisor<T>(n,tmp,result);});
      }
      // wait for threads to complete
      while(!isPoolEmpty())
      {
        if(false == result) {
          std::unique_lock<std::mutex>
            poolMutex(_threadFunctionPoolMutex, std::defer_lock);
            poolMutex.lock();
            // remove further unneeded work for the thread pool
            // N.B. threads should make sure to retrieve functions by copying
            //  and removing
          _threadFunctionPool.clear();
          return false;
        }
        std::this_thread::sleep_for(std::chrono::microseconds(50));
      }
      return result;
  }
  // -----------------------------poolFunctionCall------------------------------
  // function that pool threads are running on.  Gets stuff to do from the
  //  function pool when needed, sleeps at other times
  void poolFunctionCall() {
    while(!_shutdownPool) {
      std::unique_lock<std::mutex>
        poolMutex(_threadFunctionPoolMutex, std::defer_lock);
      poolMutex.lock();
      // no work, go to sleep
      if(_threadFunctionPool.empty()) {
        std::this_thread::sleep_for(std::chrono::microseconds(50));
      }
      else {
        // get function then remove it
        std::function<void()> f = _threadFunctionPool.back();
        _threadFunctionPool.pop_back();
        poolMutex.unlock();
        f();
      }
    }
  }

// -----------------------------------members-----------------------------------
private:
  std::vector<T>                      _primeResults;
  std::vector<std::thread>            _threadPool;
  std::vector<std::function<void()>>  _threadFunctionPool;
  std::mutex                          _threadFunctionPoolMutex;
  bool                                _shutdownPool               = false;
};
// ============================End PrimeCalcThreaded============================

#define MAX_N 600851475143
//#define MAX_N 13195
typedef uint64_t ntype;
int main(int argc, char** argv) {
  try {
    std::cout << "Threads avaliable: " << NUMBER_THREADS
              << std::endl;
    PrimeCalcThreaded<ntype> calc;
    // only need to calculate until greatest possible common divisor
    std::vector<ntype> results;
    results.push_back(1); // 1 is always a common denominator
    std::cout << "Calculating primes, reducing by found factors\n"
              << "Found primes: " << std::endl;
    std::cout << "1";
    ntype mainCount = MAX_N;
    ntype nextPrime = 1;
    while(mainCount > nextPrime) {
      ntype nextPrime = calc.calculateNext(true);
      // if found a divisor, store it and divide mainCount to make out lives
      //  easier
      if((mainCount % nextPrime) == 0) {
        mainCount /= nextPrime;
        results.push_back(nextPrime);
      }
    }

    std::cout << "\nPrime factors of " << MAX_N << ": \n";
    for (auto it = results.begin(); it != results.end(); it++) {
      std::cout << *it << "\n";
    }
    std::cout << "Max prime factor: " << results.back() << std::endl;
    cmdPause();
  }
  catch(std::exception& e) {
    std::cout << e.what() << std::endl;
  }
  return 0;
}
