// ================================Problem 2 main===============================
// Source code to attempt to solve Project Euler problem 2
//  see https://projecteuler.net/problem=2
//  Problem description: find the sum of all even fibonacci numbers below 4
//   million
// -----------------------------------------------------------------------------
// Copyright 2018 David S. H. Turner
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.
// =============================================================================

#include <vector>
#include <cstdint>      // uint64_t
#include <algorithm>    // sort
#include <set>
#include <limits>
#include <iostream>

// ===============================Start Functions===============================
// -----------------------Start getNaturalNumberMultiples-----------------------
/// \param[in] multiplier  natural number to get multiples of
/// \param[in] min         natural number to start calculating multiples at
/// \param[in] max         natural number to stop calculating multiples at
/// \return                vector whose elements are found multiples of
///                        multiplier between min and max
template <typename T>
std::vector<T>
getNaturalNumberMultiples(const T& multiplier, const T& min, const T& max) {
  static_assert(std::is_unsigned<T>());

  T minCount   = min % multiplier;
  T maxCount   = max % multiplier;
  // round up to the nearest multiple
  T startCount = (0 == minCount) ?
                         min/multiplier :
                         (min+(multiplier-minCount))/multiplier;
  // round down to the nearest multiple
  T endCount   = (max-maxCount)/multiplier;
  // store multiples
  std::vector<T> result;
  for(T i = startCount; i < (endCount+1); i++) {
    result.push_back(i*multiplier);
  }
  return result;
}
// ------------------------End getNaturalNumberMultiples------------------------

// --------------------------------Start cmdPause-------------------------------
/// Pauses command line to wait for input
void cmdPause(){
  std::cout << "Press ENTER to continue" << std::endl;
  std::string temp;
  std::getline(std::cin, temp);
}
// ---------------------------------End cmdPause--------------------------------
// ================================End Functions================================

#define MAX_N 999
typedef uint64_t ntype;
int main(int argc, char** argv) {
  std::vector<ntype> multiples3
    = getNaturalNumberMultiples<uint64_t>(3,0,MAX_N);
  std::vector<ntype> multiples5
    = getNaturalNumberMultiples<uint64_t>(5,0,MAX_N);
  // sets do not allow for duplicates, and comes sorted (yay!)
  std::set<ntype> uniqueMultiples;
  uniqueMultiples.insert(multiples3.begin(), multiples3.end());
  uniqueMultiples.insert(multiples5.begin(), multiples5.end());

  ntype result = 0;
  for(auto it = uniqueMultiples.begin(); it != uniqueMultiples.end(); it++) {
    // check for overflow
    if(result > (std::numeric_limits<ntype>::max() - *it)) {
      std::cout << "Overflow has occurred when adding" << result
                << " and " << *it
                << ", terminating" << std::endl;
      break;
    }
    result += *it;
  }
  std::cout << "Addition of unique multipliers of 3 and 5 under 1000 : "
            << result << std::endl;
  cmdPause();
  return 0;
}
