// ================================Problem 1 main===============================
// Source code to attempt to solve Project Euler problem 2
//  see https://projecteuler.net/problem=2
//  Problem description: sum all even-valued numbers of the Fibonacci sequence
//    that are under 4 million
// -----------------------------------------------------------------------------
// Copyright 2018 David S. H. Turner
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.
// =============================================================================

#include <iostream>
#include <vector>
#include <stdexcept>    // overflow_error

// =================================Start isEven================================
struct IsEven {
public:
// ----------------------------------functions----------------------------------
  // --------------------------------operator(T)--------------------------------
  /// \return if the value provided is even
  template <typename T>
  bool operator() (T value) {
    // neglect to consider unsigned variables
    static_assert(std::is_unsigned<T>());
    // checks the first bit, odd values will always have this set
    _isEven = (0 == (value & 1));
    return _isEven;
  }
// -----------------------------------members-----------------------------------
public:
  /// \var if the last value provided to this struct was even
  bool _isEven = false;
} isEven;
// ==================================End isEven=================================

// =============================Start FibonacciCalc=============================
// as we are dealing with even numbers, doesn't matter if we start with 1,2 or
//  1,1
#define START_1 1
#define START_2 1
template <typename T>
class FibonacciCalc {
  // -----------------------------------asserts-----------------------------------
  static_assert(std::is_unsigned<T>());
// ----------------------------------functions----------------------------------
public:
  // --------------------------------constructor--------------------------------
  FibonacciCalc() {
    // first two numbers in the sequence are 1
    _fibResults.push_back(START_1);
    _fibResults.push_back(START_2);
  }
  // -------------------------------calculateNext-------------------------------
  /// \return the next value in the Fibonacci sequence
  T calculateNext() {
    auto back = _fibResults.end();
    // add the last 2 numbers calculated from the sequence and store the result
    T first   = *(back-1);
    T second  = *(back-2);
    T result  = first + second;
    if(first > (std::numeric_limits<T>::max() - second)) {
      throw std::overflow_error("Overflow in Fibonacci Calculator");
    }
    _fibResults.push_back(result);
    return result;
  }
  // -------------------------------calculateNext-------------------------------
  /// \return the first calculated value greater than the provided value
  T calculateUntilSurpassed(T value) {
    while(value > calculateNext());
    return *(_fibResults.end());
  }
  // ------------------------------getCurrentResult-----------------------------
  /// \return the last calculated Fibonacci number
  T getCurrentResult() {
    return *(_fibResults.end());
  }
  // ------------------------------getCurrentResult-----------------------------
  /// \return vector containing all calculated Fibonacci numbers
  std::vector<T> getAllResults() {
    return _fibResults;
  }
// -----------------------------------members-----------------------------------
private:
  std::vector<T> _fibResults;
};
// ==============================End FibonacciCalc==============================

// ===============================Start Functions===============================
// --------------------------------Start cmdPause-------------------------------
/// Pauses command line to wait for input
void cmdPause(){
  std::cout << "Press ENTER to continue" << std::endl;
  std::string temp;
  std::getline(std::cin, temp);
}
// ---------------------------------End cmdPause--------------------------------
// ================================End Functions================================

#define MAX_N 4000000
typedef uint64_t ntype;
int main(int argc, char** argv) {
  try {
    FibonacciCalc<ntype> calculator;
    calculator.calculateUntilSurpassed(MAX_N);
    std::vector<ntype> fibNumbers = calculator.getAllResults();
    // prints obtained numbers
    // for(auto it = fibNumbers.begin(); it != fibNumbers.end(); it++) {
    //   std::cout << *it << ",";
    // }
    // std::cout << std::endl;
    // remove last element, as it will be over MAX_N
    fibNumbers.pop_back();
    // add all even numbers
    ntype result = 0;
    for(auto it = fibNumbers.begin(); it != fibNumbers.end(); it++) {
      if(isEven(*it)) {
        ntype curr = *it;
        // check for overflows
        if(curr > (std::numeric_limits<ntype>::max() - result)) {
          throw std::overflow_error("Overflow in even fibonacci addition");
        }
        result += curr;
      }
    }
    std::cout << "Addition of even fibonacci numbers under " << MAX_N
              << ": " << result << std::endl;
    cmdPause();
  }
  catch(std::exception& e) {
    std::cout << e.what() << std::endl;
  }
  return 0;
}
